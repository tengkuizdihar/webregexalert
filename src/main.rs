use chrono::{Datelike, Timelike, Utc};
use clap::{App, Arg, ArgMatches};
use regex::RegexBuilder;
use rodio::Source;
use std::{fs::File, u64};
use std::io::BufReader;
use tokio::time::{sleep, Duration};

fn application_configuration() -> ArgMatches<'static> {
    App::new("Web Regex Alert")
        .about("Simple application that will alert the user when finding a regex pattern in an html document. If found, just press CTRL+C to end the program.")
        .author("Tengku Izdihar <tengkuizdihar:matrix.org>")
        .version("1.0")
        .arg(Arg::with_name("regex").short("r").value_name("REGEXVALUE").help("The regex value that the application will match to such as 'tengku izdihar'").default_value("tengku izdihar"))
        .arg(Arg::with_name("url").short("u").value_name("WEBSITEURLHTTPS").help("The url of the website such as 'https://www.gitlab.com/'").default_value("https://sidang.cs.ui.ac.id/"))
        .arg(Arg::with_name("time").short("t").value_name("TIMESECOND").help("How many second the request will be done again, should always be positive").default_value("60"))
        .get_matches()
}

fn play_alarm_infinitely() {
    let (_stream, stream_handle) = rodio::OutputStream::try_default().unwrap();

    // Load a sound from a file, using a path relative to Cargo.toml
    let file = File::open("resource/alarm.wav").unwrap();
    let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
    let source = source.repeat_infinite();
    let _ = stream_handle.play_raw(source.convert_samples());

    // The sound plays in a separate audio thread,
    // so we need to keep the main thread alive while it's playing.
    // Press ctrl + C to stop the process once you're done.
    loop {
        continue;
    }
}

fn give_date_string() -> String {
    let now = Utc::now().naive_local();
    format!(
        "{:02}-{:02} {:?} || {:02}:{:02}:{:02}",
        now.month(),
        now.day(),
        now.weekday(),
        now.hour(),
        now.minute(),
        now.second()
    )
}

async fn get_html_text_from(url: &str) -> Result<String, Box<dyn std::error::Error>> {
    let body = reqwest::get(url).await?.text().await?;
    Ok(body)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let app_config = application_configuration();
    let regex_query = app_config.value_of("regex").unwrap();
    let url = app_config.value_of("url").unwrap();
    let wait_second: u64 = app_config.value_of("time").unwrap().parse::<_>().unwrap();
    let rx_machine = RegexBuilder::new(regex_query)
        .case_insensitive(true)
        .build()
        .expect("Should be a valid regex");

    // Welcoming status
    println!(
        "Made by Tengku Izdihar, in about an hour.
===============================================================================

Program is starting and will scrap the url and match it to the regex chosen.
Regex chosen is : {}
Url chosen is   : {}

===============================================================================
",
        regex_query, url
    );

    loop {
        let result = get_html_text_from(url).await?;
        let is_sentence_exist = rx_machine.is_match(result.as_str());

        // Simple logging to stdout
        println!(
            "{} => the status of finding the sentence is: {}",
            give_date_string(),
            is_sentence_exist
        );
        if is_sentence_exist {
            println!("SENTENCE FOUND! ALARM SOUND!");
            play_alarm_infinitely();
        }

        // Wait for 5 minute
        sleep(Duration::from_secs(wait_second)).await
    }
}
