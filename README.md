# Web Regex Alert
An application that will warn the user when the regex is matched in the body of the HTML of a website. It's quick, dirty, and functional. I made this in an hour, bored when waiting for my thesis defence's schedule. Use the --help command to see what you can do in the application.

# Credit
Tengku Izdihar as the programmer